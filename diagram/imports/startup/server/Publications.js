import {Meteor} from 'meteor/meteor'
import {SimpleChats} from '../../api/simpleChat/SimpleChatCollection'
import {Tasks} from '../../api/task/TaskCollection'

/** Publish all the collections you need. */
SimpleChats.publish()
Tasks.publish()

// このシステムにアカウントを持つユーザは基本的に信頼できるものとする
Meteor.publish('allUserData', function() {
  if (this.userId) {
    return Meteor.users.find({}, {fields: {emails: 1, profile: 1, username: 1}})
  }
  return this.ready()
})

/** Need this for the alanning:roles package */
Meteor.publish(null, function() {
  if (this.userId) {
    return Meteor.roleAssignment.find({'user._id': this.userId})
  }
  return this.ready()
})
