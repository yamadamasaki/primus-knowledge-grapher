import React from 'react'
import PropTypes from 'prop-types'
import {Meteor} from 'meteor/meteor'
// import 'semantic-ui-css/semantic.css'
import {Roles} from 'meteor/alanning:roles'
import {BrowserRouter as Router, Redirect, Route, Switch} from 'react-router-dom'
import {ToastProvider} from 'react-toast-notifications'
import NavBar from '../components/NavBar'
import Footer from '../components/Footer'
import Landing from '../pages/Landing'
import NotFound from '../pages/NotFound'
import Signin from '../pages/Signin'
import Signout from '../pages/Signout'
import KGKanbanPage from '../pages/KGKanbanPage'

const styleLink = document.createElement("link")
styleLink.rel = "stylesheet"
styleLink.href = "https://cdn.jsdelivr.net/npm/semantic-ui/dist/semantic.min.css"
document.head.appendChild(styleLink)

/** Top-level layout component for this application. Called in imports/startup/client/startup.jsx. */
const App = () => (
      <Router>
          <ToastProvider>
            <NavBar/>
            <Switch>
              <ProtectedRoute exact path="/" component={KGKanbanPage}/>
              <Route path="/signin" component={Signin}/>
              <ProtectedRoute path="/signout" component={Signout}/>
              <Route component={NotFound}/>
            </Switch>
            <Footer/>
          </ToastProvider>
      </Router>
  )

/**
 * ProtectedRoute (see React Router v4 sample)
 * Checks for Meteor login before routing to the requested page, otherwise goes to signin page.
 * @param {any} { component: Component, ...rest }
 */
const ProtectedRoute = ({component: Component, ...rest}) => (
    <Route
        {...rest}
        render={(props) => {
          const isLogged = Meteor.userId() !== null
          return isLogged ?
              (<Component {...props} />) :
              (<Redirect to={{pathname: '/signin', state: {from: props.location}}}/>
              )
        }}
    />
)

/**
 * AdminProtectedRoute (see React Router v4 sample)
 * Checks for Meteor login and admin role before routing to the requested page, otherwise goes to signin page.
 * @param {any} { component: Component, ...rest }
 */
const AdminProtectedRoute = ({component: Component, ...rest}) => (
    <Route
        {...rest}
        render={(props) => {
          const isLogged = Meteor.userId() !== null
          const isAdmin = Roles.userIsInRole(Meteor.userId(), 'admin')
          return (isLogged && isAdmin) ?
              (<Component {...props} />) :
              (<Redirect to={{pathname: '/signin', state: {from: props.location}}}/>
              )
        }}
    />
)

/** Require a component and location to be passed to each ProtectedRoute. */
ProtectedRoute.propTypes = {
  component: PropTypes.oneOfType([
    PropTypes.func.isRequired,
    PropTypes.object.isRequired,
  ]),
  location: PropTypes.object,
}

/** Require a component and location to be passed to each AdminProtectedRoute. */
AdminProtectedRoute.propTypes = {
  component: PropTypes.oneOfType([
    PropTypes.func.isRequired,
    PropTypes.object.isRequired,
  ]),
  location: PropTypes.object,
}

export default App
