import React from 'react'
import {useParams} from 'react-router'
import {useTranslation} from 'react-i18next'
import {Helmet} from 'react-helmet'
import SyncFusionKanban from '../components/SyncFusionKanban.jsx'

const KGKanbanPage = () => {
  const {t} = useTranslation()
  const params = useParams()

  return (
    <>
      <Helmet><title>Kanban</title></Helmet>
      <SyncFusionKanban/>
    </>
  )
}

export default KGKanbanPage
