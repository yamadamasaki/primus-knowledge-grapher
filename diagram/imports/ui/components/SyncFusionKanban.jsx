import React, {useRef} from 'react'
import '@syncfusion/ej2-base/styles/material.css'
import '@syncfusion/ej2-buttons/styles/material.css'
import '@syncfusion/ej2-layouts/styles/material.css'
import '@syncfusion/ej2-dropdowns/styles/material.css'
import '@syncfusion/ej2-inputs/styles/material.css'
import '@syncfusion/ej2-navigations/styles/material.css'
import '@syncfusion/ej2-popups/styles/material.css'
import '@syncfusion/ej2-react-kanban/styles/material.css'
import {addClass, extend} from '@syncfusion/ej2-base'
import {KanbanComponent, ColumnDirective, ColumnsDirective} from '@syncfusion/ej2-react-kanban'
import './SyncFusionKanban.css'
import * as dataSource from './SyncFusionKanbanDatasource.json'
import {Button} from 'semantic-ui-react'
import {useTranslation} from 'react-i18next'

const SyncFusionKanban = () => {
  const {t} = useTranslation()
  const data = extend([], dataSource.cardData, null, true)
  const fields = [
    {text: 'ID', key: 'Title', type: 'TextBox'},
    {key: 'Status', type: 'DropDown'},
    {key: 'Assignee', type: 'DropDown'},
    {key: 'RankId', type: 'TextBox'},
    {key: 'Summary', type: 'TextArea'},
  ]

  const cardRendered = args => addClass([args.element], args.data.Priority)

  const getString = assignee => assignee.match(/\b(\w)/g).join('').toUpperCase()

  const columnTemplate = props => {
    return (
      <div className="header-template-wrap">
        <div className={'header-icon e-icons ' + props.keyField}/>
        <div className="header-text">{props.headerText}</div>
      </div>
    )
  }

  const cardTemplate = props => {
    return (
        <div className={'card-template'}>
          <div className="e-card-header">
            <div className="e-card-header-caption">
              <div className="e-card-header-title e-tooltip-text">{props.Title}</div>
            </div>
          </div>
          <div className="e-card-content e-tooltip-text">
            <div className="e-text">{props.Summary}</div>
          </div>
          <div className="e-card-custom-footer">
            {props.Tags.split(',').map((tag, index) => <div key={index} className="e-card-tag-field e-tooltip-text">{tag}</div>)}
            <div className="e-card-avatar">{getString(props.Assignee)}</div>
          </div>
        </div>
    )
  }

  const cardSettings = {
    headerField: 'Title',
    template: cardTemplate,
    selectionType: 'Multiple',
  }

  const kanban = useRef()
  
  const addTask = () => {
    const cardIds = kanban.current.kanbanData.map((obj) => parseInt(obj.Id.replace('Task ', ''), 10));
    const cardCount = Math.max.apply(Math, cardIds) + 1;
    const cardDetails = { Id: "Task " + cardCount, Status: "Open", Priority: "Normal", Assignee: "Andrew Fuller", Estimate: 0, Tags: "", Summary: "" };
    kanban.current.openDialog('Add', cardDetails);
  }

  return (
    <div className="schedule-control-section">
      <div className="col-lg-12 control-section">
        <div className="control-wrapper">
          <Button onClick={addTask}>{t('Add a New Task')}</Button>
          <KanbanComponent
              id="kanban" cssClass="kanban-overview" keyField="Status" dataSource={data}
              enableTooltip={true} swimlaneSettings={{keyField: 'Assignee'}} cardSettings={cardSettings}
              dialogSettings={{fields: fields}} cardRendered={cardRendered} ref={kanban}>
            <ColumnsDirective>
              <ColumnDirective headerText="To Do" keyField="Open" allowToggle={true} template={columnTemplate}/>
              <ColumnDirective headerText="In Progress" keyField="InProgress" allowToggle={true} template={columnTemplate}/>
              <ColumnDirective headerText="Testing" keyField="Testing" allowToggle={true} template={columnTemplate}/>
              <ColumnDirective headerText="Done" keyField="Close" allowToggle={true} template={columnTemplate}/>
            </ColumnsDirective>
          </KanbanComponent>
        </div>
      </div>
    </div>
  )
}

export default SyncFusionKanban
