import React from 'react'
import {Meteor} from 'meteor/meteor'
import {useTracker} from 'meteor/react-meteor-data'
import {NavLink} from 'react-router-dom'
import {Dropdown, Header, Icon, Menu} from 'semantic-ui-react'
import {useTranslation} from 'react-i18next'

/** The NavBar appears at the top of every page. Rendered by the App Layout component. */
const NavBar = () => {
  const {t} = useTranslation()
  const user = useTracker(() => Meteor.user())
  const currentUser = user ? user.username : ''
  const {applicationName} = Meteor.settings.public
  const menuStyle = {marginBottom: '10px'}

  return (
      <Menu style={menuStyle} attached="top" borderless inverted>
        {currentUser && (
            <Menu.Item id='navbar-toggle-sidebar'>
              <Icon name="bars"/>
            </Menu.Item>
        )}
        <Menu.Item as={NavLink} activeClassName="" exact to="/">
          <Header inverted as="h1">
            {applicationName}
            <Header.Subheader>Kanban</Header.Subheader>
          </Header>
        </Menu.Item>
        <Menu.Item position="right">
          {currentUser === '' ? (
              <Dropdown text={t('Login')} pointing="top right" icon={'user'}>
                <Dropdown.Menu>
                  <Dropdown.Item icon="user" text={t('Sign In')} as={NavLink} exact to="/signin"/>
                </Dropdown.Menu>
              </Dropdown>
          ) : (
              <Dropdown text={currentUser} pointing="top right" icon={'user'}>
                <Dropdown.Menu>
                  <Dropdown.Item icon="sign out" text={t('Sign Out')} as={NavLink} exact to="/signout"/>
                </Dropdown.Menu>
              </Dropdown>
          )}
        </Menu.Item>
      </Menu>
  )
}

export default NavBar
