# ユーザ・アカウントに関する仕様

* KnowledgeGrapher アプリケーションを開く
* KnowledgeGrapher ブランドが表示された

本当は KnowledgeGrapher ブランドのリンクまでチェックするといいかもしれない.

## 自分でアカウントを作る

空のシステムでは, デフォルトの初期アカウントが存在する (admin@foo.com/changeme, john@foo.com/changeme)

既に同じ名前 (名前がない場合にはメイル・アドレス) のアカウントが存在しないことを前提としている.

* ユーザ名 "yamadamasaki" のアカウントをメイル・アドレス "yamada@foo.com", パスワード "changeme" で作る
* "programs" ページに遷移した
* "yamada@foo.com" のアカウントからサインアウトする
* "signout" ページに遷移した
* メイル・アドレス "yamada@foo.com" にパスワード "changeme" でサインインする
- ToTest: "自分が admin でないことを確認する"
* "yamada@foo.com" のアカウントからサインアウトする
* ユーザ名 "yamadamasaki" のアカウントをメイル・アドレス "yamada@foo.com", パスワード "changeme" で作る
* テキスト "アカウントを作成できませんでした" が表示された

## 自分のプロファイルを設定する

* メイル・アドレス "yamada@foo.com" にパスワード "changeme" でサインインする
* "profile" ページに行く
* プロファイルをユーザ名 "yamada.masaki", アバタ "./files/avataaars-01.svg" に設定する
* テキスト "ユーザ・プロファイルを更新しました" が表示された

### ToDo

- ToTest: "Admin アカウントでログインした場合には, Admin 権限があることを確認する"
- ToImplement: "Admin アカウントを作る - 多分未実装"
- ToImplement: "その他のアカウント作成方法 - 未実装"
- ToImplement: "Admin によるアカウント管理 - 未実装"
