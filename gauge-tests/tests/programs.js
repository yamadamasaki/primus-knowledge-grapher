const {click, link, currentURL, goto} = require('taiko')
const config = require('./config')

step('左サイドバーをトグルする', async () => {
  await click(link({id: 'navbar-toggle-sidebar'}))
})

const last = array => array[array.length-1]

step('コンポネント <コンポネント> を使ってセッション <セッション> を表示する', async (コンポネント, セッション) => {
  const currentUrl = await currentURL()
  const programId = last(currentUrl.split('/'))
  await goto(`${config.ROOT_URL}/sessions/${programId}/${コンポネント}/${セッション}`)
})
