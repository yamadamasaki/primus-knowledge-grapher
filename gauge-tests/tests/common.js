const {
  goto, click, text, textBox, focus, write, into, currentURL, fileField, attach, to, dropDown, link, $, screenshot,
  button,
} = require('taiko')
const assert = require('assert')
const fs = require('fs')
const config = require('./config')

const {ROOT_URL} = config

step('Say <word>', async word => {
  console.log({word})
})

step('KnowledgeGrapher アプリケーションを開く', async () => goto(ROOT_URL))

step('KnowledgeGrapher ブランドが表示された', async () => assert.ok(await text('KnowledgeGrapher').exists(0, 0)))

step('<名前> ボタンをクリックする', async 名前 => await click(名前))

step('<フィールド名> フィールドに <テキスト> を入力する', async (フィールド名, テキスト) => {
  await focus(textBox({name: フィールド名}))
  await write(テキスト, into({name: フィールド名}))
})

step('<URL> ページに遷移した', async URL => assert.equal(await currentURL(), `${ROOT_URL}/${URL}`))
step('<URL> ページ (正規表現) に遷移した', async URL => assert.match(await currentURL(), new RegExp(`${ROOT_URL}/${URL}`)))

step('テキスト <テキスト> が表示された', async テキスト => assert.ok(await text(テキスト).exists(0, 0)))

step('<URL> ページに行く', async URL => goto(`${ROOT_URL}/${URL}`))

step('ファイル・フィールド <フィールド名> に <ファイル名> を入力する', async (フィールド名, ファイル名) => {
  await focus(fileField({name: フィールド名}))
  await attach(ファイル名, to(fileField({name: フィールド名})))
})

step('フィールド <フィールド名> にテキスト <テキスト> が表示された', async (フィールド名, テキスト) => {
  assert.equal(await textBox({name: フィールド名}).value(), テキスト)
})

step('<ドロップダウン> ドロップダウンから <項目> を選択する', async (ドロップダウン, 項目) => {
  await dropDown({name: ドロップダウン}).select(項目)
})

step('<フィールド名> フィールドにファイル <ファイル名> からテキストを読み込む', async (フィールド名, ファイル名) => {
  const data = fs.readFileSync(`./files/${ファイル名}`, 'utf8')
  await focus(textBox({name: フィールド名}))
  await write(data, into({name: フィールド名}))
})

step('<リンク> リンクをクリックする', async リンク => {
  await focus(link(リンク))
  await click(link(リンク))
})

step('スクリーンショットを撮る', async () => await screenshot())

step('セレクタ <セレクタ> をクリックする', async セレクタ => await click($(セレクタ), {position: 'right'}))

step('リンク <リンク> が存在した', async リンク => await link(リンク).exists())

step('<ボタン名> ボタンがあればクリックする', async ボタン名 => {
  if (await button(ボタン名).exists()) await click(button(ボタン名))
})
