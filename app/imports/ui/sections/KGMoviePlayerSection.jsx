import React, {useEffect, useState} from 'react'
import {useTracker, withTracker} from 'meteor/react-meteor-data'
import {useTranslation} from 'react-i18next'
import {Button, Form, Loader, Message} from 'semantic-ui-react'
import {movieDefineMethod, movieUpdateMethod} from '../../api/movie/MovieCollection.methods'
import {Meteor} from 'meteor/meteor'
import {isPermitted, KGIfIHave} from '../components/KGIfIHave'
import {useToasts} from 'react-toast-notifications'
import ReactPlayer from 'react-player'
import {Movies} from '../../api/movie/MovieCollection'
import {convertFromRaw, EditorState} from 'draft-js'

const sectionStyle = {
  padding: '1rem 2rem',
  borderLeft: '6px double royalblue',
  borderRight: '6px double royalblue',
  margin: '1rem 0rem',
}

const KGMoviePlayerSection = ({documentLoading, document, selector, canRead, canWrite}) => {
  const currentUser = useTracker(() => Meteor.user())
  const {addToast} = useToasts()
  const {t} = useTranslation()

  const [source, setSource] = useState('')
  const [url, setUrl] = useState('')

  useEffect(() => {
    if (!documentLoading) {
      setSource(document?.source || '')
      setUrl(document?.source || '')
    }
  }, [setSource, document, documentLoading])

  if (!isPermitted(currentUser, canRead))
    return <Message color="yellow">{t('This section is not published')}</Message>

  const showError = message => addToast(message, {appearance: 'error', autoDismiss: false})
  const showSuccess = () => addToast(t('Success'), {appearance: 'success', autoDismiss: true})
  const onError = error => error ? showError(error.message) : showSuccess()

  const save = () => {
    !document || !document._id ?
        movieDefineMethod.call({...selector, source}, onError) :
        movieUpdateMethod.call({_id: document._id, ...selector, source}, onError)
    setUrl(source)
  }

  return (
      documentLoading ? <Loader/> : (
          <div style={sectionStyle}>
            <ReactPlayer url={url}/>
            <KGIfIHave permission={canWrite}>
              <Form onSubmit={save}>
                <Form.Field>
                  <label>{`${t('Movie')} URL`}</label>
                  <input value={source} onChange={e => setSource(e.target.value)} type="url"/>
                </Form.Field>
                <Button type="submit">{t('Settings')}</Button>
              </Form>
            </KGIfIHave>
          </div>
      )
  )
}

export default withTracker(({programId, sessionId, subsession, id, canRead, canWrite}) => {
  const selector = id || (subsession ? {programId, sessionId, subsession} : {programId, sessionId})
  const documentLoading = !Movies.subscribe(Movies.getChannels().allWithMeta).ready()
  const document = Movies.findOne(selector)

  return {documentLoading, document, selector, canRead, canWrite}
})(KGMoviePlayerSection)
