import {Movies} from './MovieCollection'
import {ValidatedMethod} from 'meteor/mdg:validated-method'
import {Meteor} from 'meteor/meteor'
import {check} from 'meteor/check'

export const movieDefineMethod = new ValidatedMethod({
  name: 'movies.define',
  validate: Movies.getSchema().validator(),
  run: obj => Meteor.isServer ? Movies.define(obj) : true,
})

export const movieUpdateMethod = new ValidatedMethod({
  name: 'movies.update',
  validate: Movies.getSchema().validator(),
  run: doc => Meteor.isServer ? Movies.update(doc._id, doc) : true,
})

export const movieDeleteMethod = new ValidatedMethod({
  name: 'movies.delete',
  validate: args => check(args, String),
  run: id => Meteor.isServer ? Movies.remove(id) : true,
})
