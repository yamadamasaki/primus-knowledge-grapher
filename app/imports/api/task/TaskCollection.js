import BaseCollection from '../base/BaseCollection'
import SimpleSchema from 'simpl-schema'

const schema = {
  _id: {type: String, optional: true},
  createdAt: {type: Date, optional: true},
  updatedAt: {type: Date, optional: true},
  owner: {type: String, optional: true},

  programId: {type: String, optional: false},
  sessionId: {type: String, optional: false},
  subsession: {type: String, optional: true},

  title: {type: String, optional: true},
  tags: {type: String, optional: true},
  assignee: {type: String, optional: true},
  status: {type: String, optional: true},
  summary: {type: String, optional: true},
  dueDate: {type: Date, optional: true},
  kpi: {type: String, optional: true}, // kpi
  list: {type: Array, optional: true},
  'list.$': Object,
  'list.$.checked': {type: Boolean, defaultValue: false},
  'list.$.description': {type: String, optional: true},
}

const channels = {}

const accessibility = {
  _: {
    canRead: ['member'],
    canUpdate: ['member'],
    canCreate: ['member'],
    canDelete: ['member'],
  },
  programId: {
    canRead: ['member'],
    canUpdate: ['admin'],
    canCreate: ['member'],
  },
  sessionId: {
    canRead: ['member'],
    canUpdate: ['admin'],
    canCreate: ['member'],
  },
  subsession: {
    canRead: ['member'],
    canUpdate: ['admin'],
    canCreate: ['member'],
  },
  title: {
    canRead: ['member'],
    canUpdate: ['member'],
    canCreate: ['member'],
  },
  tags: {
    canRead: ['member'],
    canUpdate: ['member'],
    canCreate: ['member'],
  },
  assignee: {
    canRead: ['member'],
    canUpdate: ['member'],
    canCreate: ['member'],
  },
  status: {
    canRead: ['member'],
    canUpdate: ['member'],
    canCreate: ['member'],
  },
  summary: {
    canRead: ['member'],
    canUpdate: ['member'],
    canCreate: ['member'],
  },
  dueDate: {
    canRead: ['member'],
    canUpdate: ['member'],
    canCreate: ['member'],
  },
  kpi: {
    canRead: ['member'],
    canUpdate: ['member'],
    canCreate: ['member'],
  },
  list: {
    canRead: ['member'],
    canUpdate: ['member'],
    canCreate: ['member'],
  },
}

class TaskCollection extends BaseCollection {
  constructor() {
    super('Tasks', new SimpleSchema(schema), accessibility, channels)
  }
}

export const Tasks = new TaskCollection()
