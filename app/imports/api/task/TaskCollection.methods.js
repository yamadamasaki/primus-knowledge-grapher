import {Tasks} from './TaskCollection'
import {ValidatedMethod} from 'meteor/mdg:validated-method'
import {Meteor} from 'meteor/meteor'
import {check} from 'meteor/check'

export const taskDefineMethod = new ValidatedMethod({
  name: 'tasks.define',
  validate: Tasks.getSchema().validator(),
  run: obj => Meteor.isServer ? Tasks.define(obj) : true,
})

export const taskUpdateMethod = new ValidatedMethod({
  name: 'tasks.update',
  validate: Tasks.getSchema().validator(),
  run: doc => Meteor.isServer ? Tasks.update(doc._id, doc) : true,
})

export const taskDeleteMethod = new ValidatedMethod({
  name: 'tasks.delete',
  validate: args => check(args, String),
  run: id => Meteor.isServer ? Tasks.remove(id) : true,
})
